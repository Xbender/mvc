<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<html>
<head>
	<title>Spring Chocolate Store</title>
<jsp:include page="/WEB-INF/views/include/head-include.jsp" />
</head>
<body>

<div style="float:right">
	<a href="users?register">Register</a>
</div>

<h2>Featured Products:</h2>

<c:forEach var = "item" items = "${productList}">
	<c:out value="${item.name}"> </c:out>
	<br/> 
</c:forEach>

</body>
</html>
