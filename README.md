README
=======

Installation
-------------
0. Install [vagrant](https://www.vagrantup.com)
0. In your favourite SQL tool connect to PostgreSQL database:

  * host: localhost
  * port: 5433
  * database: chocolatestore
  * database username: choco_user
  * database password: choco

Usage
------
0. In console run: 'vagrant up' from project root folder
0. Enjoy :)