package com.packtpub.springmvc.chocolatestore.repository;

import com.packtpub.springmvc.chocolatestore.domain.Category;
import com.packtpub.springmvc.chocolatestore.domain.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by kostyab on 18.04.15.
 */
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    List<Product> findByFeatured(Boolean featured);

//    List<Product> findProductEntries(Product product);
    List<Product> findByNameAndCategory(String productName, Category category);

    List<Product> findByName(String productName);
}
