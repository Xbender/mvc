package com.packtpub.springmvc.chocolatestore.service;

import com.google.common.collect.Lists;
import com.packtpub.springmvc.chocolatestore.domain.PurchaseItem;
import com.packtpub.springmvc.chocolatestore.repository.PurchaseItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PurchaseItemService {

	@Autowired
	private PurchaseItemRepository purchaseItemRepository;
	
	public long countAllPurchaseItems() {
        return purchaseItemRepository.count();
    }

	public void deletePurchaseItem(PurchaseItem purchaseItem) {
		purchaseItemRepository.delete(purchaseItem);
    }

	public PurchaseItem findPurchaseItem(Long id) {
        return purchaseItemRepository.findOne(id);
    }

	public List<PurchaseItem> findAllPurchaseItems() {
        return Lists.newArrayList(purchaseItemRepository.findAll());
    }

	public List<PurchaseItem> findPurchaseItemEntries(int firstResult, int maxResults) {
        final PageRequest pageRequest = new PageRequest(firstResult, maxResults);
        return Lists.newArrayList(purchaseItemRepository.findAll(pageRequest));
    }

	public void savePurchaseItem(PurchaseItem purchaseItem) {
		purchaseItemRepository.save(purchaseItem);
    }

}
