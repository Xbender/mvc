package com.packtpub.springmvc.chocolatestore.service;

import com.google.common.collect.Lists;
import com.packtpub.springmvc.chocolatestore.domain.Customer;
import com.packtpub.springmvc.chocolatestore.domain.Product;
import com.packtpub.springmvc.chocolatestore.domain.Purchase;
import com.packtpub.springmvc.chocolatestore.domain.PurchaseItem;
import com.packtpub.springmvc.chocolatestore.repository.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Service
@Transactional
public class PurchaseService {

	@Autowired
	private PurchaseRepository purchaseRepository;
	
	public long countAllPurchases() {
        return purchaseRepository.count();
    }

	public void deletePurchase(Purchase purchase) {
		purchaseRepository.delete(purchase);
    }

	public Purchase findPurchase(Long id) {
        return purchaseRepository.findOne(id);
    }

	public List<Purchase> findAllPurchases() {
        return Lists.newArrayList(purchaseRepository.findAll());
    }

	public List<Purchase> findPurchaseEntries(int firstResult, int maxResults) {
        final PageRequest pageRequest = new PageRequest(firstResult, maxResults);
        return Lists.newArrayList(purchaseRepository.findAll(pageRequest));
    }

	public void savePurchase(Purchase purchase) {
		purchaseRepository.save(purchase);
    }

	public void savePurchase(Map<Product, Integer> cartContents, Customer purchasedBy) {
		
		Purchase purchase = new Purchase();
		Calendar now = Calendar.getInstance();
		for (Entry<Product, Integer> entry : cartContents.entrySet()) {
			PurchaseItem purchaseItem = new PurchaseItem();
			purchaseItem.setProduct(entry.getKey());
			purchaseItem.setQuantity(entry.getValue());
			purchaseItem.setOrderDate(now);
			
			purchaseItem.setPurchase(purchase);
			purchase.getPurchaseItems().add(purchaseItem);
		}
		purchase.setOrderDate(now);
		purchase.setOrderedBy(purchasedBy);
		
		this.savePurchase(purchase);
	}
}
