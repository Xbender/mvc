package com.packtpub.springmvc.chocolatestore.repository;

import com.packtpub.springmvc.chocolatestore.domain.Purchase;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by kostyab on 18.04.15.
 */
public interface PurchaseRepository extends PagingAndSortingRepository<Purchase, Long> {
}
