package com.packtpub.springmvc.chocolatestore.repository;

import com.packtpub.springmvc.chocolatestore.domain.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by kostyab on 18.04.15.
 */
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {
    Customer findByNameEquals(String name);
}
