#! /usr/bin/env bash

# Variables
POSTGRES_VERSION=9.3
DBNAME=chocolatestore
DBUSER=choco_user
DBPASSWD=choco

# APACHE TOMCAT
TOMCAT_VERSION=apache-tomcat-8.0.21
TOMCAT_INSTALATION_DIR=/usr/local/$TOMCAT_VERSION

apt-get -y update
apt-get -y upgrade

dpkg -s openjdk-7-jdk &>/dev/null || {
	apt-get -y install openjdk-7-jdk	
}


dpkg -s postgresql &>/dev/null || {

	apt-get -y install postgresql-$POSTGRES_VERSION postgresql-contrib-$POSTGRES_VERSION
	cp /vagrant/provision/pg_hba.conf /etc/postgresql/$POSTGRES_VERSION/main/pg_hba.conf
	cp /vagrant/provision/postgresql.conf /etc/postgresql/$POSTGRES_VERSION/main/postgresql.conf

	sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD '$DBPASSWD'"
	sudo -u postgres psql -c "CREATE USER $DBUSER WITH PASSWORD '$DBPASSWD'"
	sudo -u postgres psql -c "CREATE DATABASE $DBNAME ENCODING = 'UTF-8' LC_CTYPE = 'en_US.UTF-8' LC_COLLATE = 'en_US.UTF-8' OWNER $DBUSER TEMPLATE template0"

}

cp /vagrant/provision/bash_profile .bash_profile

service postgresql restart

if [ "$(ls -A $TOMCAT_INSTALATION_DIR)" ]; then
	echo "Tomcat already installed in $TOMCAT_INSTALATION_DIR"
else
    wget http://apache.ip-connect.vn.ua/tomcat/tomcat-8/v8.0.21/bin/apache-tomcat-8.0.21.tar.gz
	tar xvzf apache-tomcat-8.0.21.tar.gz
	mv apache-tomcat-8.0.21 /usr/local/
	mv /vagrant/provision/setenv.sh $TOMCAT_INSTALATION_DIR/bin
	chmod 777 $TOMCAT_INSTALATION_DIR/*
fi

